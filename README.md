# Portfolio Website

Hello and thank you for visiting my portfolio! This repository is the heart of my personal and professional journey as a Data Scientist, showcasing a curated collection of my projects and accomplishments. Here, you'll find a diverse range of work that reflects my skills, interests, and my continuous pursuit of growth in the dynamic field of technology. [Check the full site here](https://www.ofosuosei.com)

## About Me

I am Ofosu Osei, a passionate and creative developer with a focus on data science, data engineering, software engineering and web development. With a background in computer science, management information systems, and data science, my journey has been driven by my curiosity and my desire to make a meaningful impact through innovative solutions.

## Project status

The design of the portfolio website is inspired by [Hashir Shoaib](https://github.com/hashirshoaeb/home). It is hosted on the AWS S3 platfrom. As of now, the website is under active construction. I am working diligently to enrich it with more content, features and a seamless user experience.

Please check back again soon for the full site, where I will showcase a range of projects and experiences that highlight my skills and journey in the field. I'm excited to share this evolving space with you, where creativity meets functionality.

Thank you for your interest and patience! 

## References

[Hashir Shoaib](https://github.com/hashirshoaeb/home)

[Domain Name Registration and Hosting](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/getting-started-cloudfront-overview.html)

